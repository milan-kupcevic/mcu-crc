/*
 *
 * MCU CRC-16-CCITT
 *
 *
 * Copyright (C) 2017  Milan Kupcevic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * 2-clause BSD
 *
 */

/*
 *
 * INTRODUCTION
 * ------------
 * MCU CRC-16-CCITT implementation is optimized for maximum performance with
 * low number of instructions, no loops and no lookup tables which makes it
 * especially suitable for use on microcontrollers with small code memory
 * space and tiny RAM. The algorithm is implemented in a single .h file and
 * conforms to ANSI C standard.
 *
 *
 * TECHNICAL DESCRIPTION
 * ---------------------
 * The MCU CRC-16-CCITT processing steps include allocating a division
 * register, setting it to an initial value, applying the polynomial
 * division to data payload byte by byte in the least significant bit
 * to the most significant bit order by polynomial:
 *
 * x^16 + x^12 + x^5 + 1
 *
 * while the division register holds the division remainder.
 *
 * This variant of CRC-16 calculation, often called mirrored, reflected or
 * reversed, is utilized in Bluetooth, ISDN, PPP, ITU-T T.30 fax machines,
 * ITU-T X.25 networks, ID cards, proximity cards and others.
 *
 *
 * USAGE OVERVIEW
 * --------------
 * Include the header file in your project. Define as many CRC state registers
 * as you need to run at the same time. For example:
 *
 *  #include "mcu-crc16-ccitt.h"
 *
 *  unsigned short int example;
 *
 * Set the "example" CRC state register to an initial value:
 *
 *  example = 0x0000;
 *
 * Compute the data payload bytes one by one:
 *
 *  crc16_ccitt_compute(&example, chr);
 *
 * When finished, get the polynomial division remainder from the state register:
 *
 *  MyCRCremainder = 0xffff & example;
 *
 * Initialize the same state register again and use it for a new stream of data.
 *
 *  example = 0x0000;
 *
 * NOTE:
 * If required by a particular protocol convert the division remainder to
 * implementation specific final CRC result. See mcu-crc16-ccitt-example.c
 * for usage example and algorithm testing.
 *
 */

#ifndef MCU_CRC16_CCITT_H
#define MCU_CRC16_CCITT_H

static
void
crc16_ccitt_compute (
  unsigned short int *crc,
  unsigned char chr)
{
  unsigned short int sta;
  chr  = *crc
       ^ chr
       ;
  chr  = chr
       ^ chr << 4
       ;
  sta  = chr << 3
       ;
  *crc = *crc >> 8
       ^ chr >> 4
       ^ sta
       ^ sta << 5
       ;
}

#endif
