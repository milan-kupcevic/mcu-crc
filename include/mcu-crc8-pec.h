/*
 *
 * MCU CRC-8-PEC
 *
 *
 * Copyright (C) 2017  Milan Kupcevic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * 2-clause BSD
 *
 */

/*
 *
 * INTRODUCTION
 * ------------
 * MCU CRC-8-PEC implementation is optimized for maximum performance with
 * low number of instructions, no loops and no lookup tables which makes it
 * especially suitable for use on microcontrollers with small code memory
 * space and tiny RAM. The algorithm is implemented in a single .h file and
 * conforms to ANSI C standard.
 *
 *
 * TECHNICAL DESCRIPTION
 * ---------------------
 * The MCU CRC-8-PEC processing steps include allocating a division register,
 * setting it to an initial value, applying the polynomial division to data
 * payload byte by byte in the most significant bit to the least significant
 * bit order by polynomial:
 *
 * x^8 + x^2 + x + 1
 *
 * while the division register holds the division remainder.
 *
 * This variant of CRC-8 calculation is utilized in SMBus Packet Error Checking
 * protocol, I2C battery monitors, ISDN User-Network Interface, ATM Header Error
 * Control and others.
 *
 *
 * USAGE OVERVIEW
 * --------------
 * Include the header file in your project. Define as many CRC state registers
 * as you need to run at the same time. For example:
 *
 *  #include "mcu-crc8-pec.h"
 *
 *  unsigned char example;
 *
 * Set the "example" CRC state register to an initial value:
 *
 *  example = 0x00;
 *
 * Compute the data payload bytes one by one:
 *
 *  crc8_pec_compute(&example, chr);
 *
 * When finished, get the polynomial division remainder from the state register:
 *
 *  MyCRCremainder = example;
 *
 * Initialize the same state register again and use it for a new stream of data.
 *
 *  example = 0x00;
 *
 * NOTE:
 * If required by a particular protocol convert the division remainder to
 * implementation specific final CRC result. See mcu-crc8-pec-example.c
 * for usage example and algorithm testing.
 *
 */

#ifndef MCU_CRC8_PEC_H
#define MCU_CRC8_PEC_H

static
void
crc8_pec_compute (
  unsigned char *crc,
  unsigned char chr)
{
  unsigned char sta;
  chr  = *crc
       ^ chr
       ;
  sta  = chr >> 6
       ;
  chr  = chr
       ^ sta
       ^ sta >> 1
       ;
  sta  = chr << 1
       ;
  *crc = chr
       ^ sta
       ^ sta << 1
       ;
}

#endif
