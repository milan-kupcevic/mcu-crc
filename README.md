MCU CRC
=======

DESCRIPTION
-----------
MCU CRC package contains highly optimized cyclic redundancy check functions
implemented with no loops and no lookup tables which makes it especially
suitable for use in microcontrollers with small code memory space and tiny
RAM. Each algorithm variant is implemented in a single .h file and conforms
to ANSI C standard for maximum portability. <http://quarkline.net/mcu-crc/>


LICENSE
-------
The MCU CRC C library files are distributed under the terms of the 2-clause
BSD license. Tools and utilities are distributed under the terms of the GNU
General Public License version 3, or any later version. Data and documentation
are distributed under the terms of the GNU General Public License version 3, or
any later version; or alternatively under the terms of the Creative Commons
Attribution-ShareAlike 3.0 license, or a later version. See the LICENSE file
included in the package for license details. Please conform to the license
requirements or contact the author if your needs would be better served by a
commercial license.


DATA PROCESSING
---------------
The MCU CRC processing steps include allocating a division register, setting it
to an initial value, applying the polynomial division to data payload byte by
byte while the division register holds the division remainder. The polynomial
division remainder is the CRC result in most cases.

As may be required by some protocols, the final CRC result producing step
might involve additional transformation of the polynomial division remainder
to produce implementation specific final CRC result; or setting the division
register to implementation specific initial value. See usage examples for
more information.


MCU CRC-8-PEC
-------------
This variant of CRC-8 calculation is utilized in SMBus Packet Error Checking
protocol, I2C battery monitors, ISDN User-Network Interface, ATM Header Error
Control and others. It is dividing the data payload in the most significant
bit first order by polynomial:

  x^8 + x^2 + x + 1

When compiled for amd64 architecture with gcc -O2 optimization option this
implementation executes 12 amd64 machine instructions per byte of processed
data; with avr-gcc -Os option it takes 14 AVR machine instructions.


MCU CRC-16-CCITT
----------------
CRC-16 ccitt calculation, often called mirrored, reflected or reversed,
is utilized in Bluetooth, ISDN, PPP, ITU-T T.30 fax machines, ITU-T X.25
networks, ID cards, proximity cards and others. It is dividing the data
payload in the least significant bit first order by polynomial:

  x^16 + x^12 + x^5 + 1

When compiled for amd64 architecture with gcc -O3 optimization option this
implementation executes 13 amd64 machine instructions per byte of processed
data; with avr-gcc -O3 option it takes 25 AVR machine instructions.


MCU CRC-16-CCITT-MSBF
---------------------
This variant of CRC-16 calculation is utilized in GSM cellphone networks, RFID
tags, MultiMedia memory cards and others. It is dividing the data payload in
the most significant bit first order by polynomial:

  x^16 + x^12 + x^5 + 1

When compiled for amd64 architecture with gcc -O3 optimization option this
implementation executes 15 amd64 machine instructions per byte of processed
data; with avr-gcc -O3 option it takes 27 AVR machine instructions.


COMPILE AND RUN USAGE EXAMPLES
------------------------------
Get the latest package release from <http://quarkline.net/mcu-crc/download/> or
see unreleased files at <https://gitlab.com/milan-kupcevic/mcu-crc.git>.

Review usage example and algorithm testing .c files complementing each CRC
variant header file.

To compile the usage examples using gratis Microsoft Visual C++ Build Tools,
open developer command prompt window, unpack the source package in a directory,
`cd` into it and type:

     nmake

To compile the usage examples using standard building tools freely available
in Linux and BSD distributions, gratis "Command Line Tools for Xcode" on Mac
or free MSYS2 command prompt with MinGW-w64 on MS Windows, unpack the source
package in a directory, `cd` into it and type:

     ./configure
     make


QUICK START USAGE GUIDE
-----------------------
Locate and read the usage overview for the particular CRC variant at the
beginning of its header file. Review the usage example and algorithm testing
.c file complementing the CRC variant header file. Choose a CRC variant that
fits your needs and include its header file in your project.


----------------------------------------------------------------------------
Copyright (C) 2017, 2018  Milan Kupcevic

Copying and distribution of this file are permitted under the terms of the
GNU General Public License version 3, or any later version; or alternatively
under the terms of the Creative Commons Attribution-ShareAlike 3.0 license,
or a later version. GPLv3+ or CC-BY-SA-3.0+

----------------------------------------------------------------------------
