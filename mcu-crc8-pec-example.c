/*
 *
 * MCU CRC-8-PEC usage example
 *
 *
 * Copyright (C) 2017  Milan Kupcevic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * 2-clause BSD
 *
 */

/* *****************************************************************
 *
 * USAGE EXAMPLE
 * -------------
 * Compile this example and test it by typing:
 *
 *  printf "123456789" | ./mcu-crc8-pec-example
 *
 *  mySMBusCRC result: f4
 *  myATMCRC remainder: f4
 *  ITU ATM HEC CRC result: a1
 *
 * or in Windows command shell:
 *
 *  echo | set /p ="123456789" | mcu-crc8-pec-example.exe
 *
 *  mySMBusCRC result: f4
 *  myATMCRC remainder: f4
 *  ITU ATM HEC CRC result: a1
 *
 */

/* This example needs stdio.h to allow for reading data from stdin and
 * printing the division remainder and CRC result out. The stdio.h is
 * not needed for the CRC computing itself.  */
#include <stdio.h>

/* include the MCU CRC-8-PEC implementation */
#include "mcu-crc8-pec.h"

int
main()
{
  int input;

  /* set the CRC register named "mySMBusCRC" to default initial state */
  unsigned char mySMBusCRC = 0x00;

  /* set the CRC register named "myATMCRC" to default initial state */
  unsigned char myATMCRC = 0x00;

  while ((input = getchar()) != EOF)
  {
    /* compute the data payload byte by byte using "mySMBusCRC" register */
    crc8_pec_compute(&mySMBusCRC, input);

    /* compute the data payload byte by byte using "myATMCRC" register */
    crc8_pec_compute(&myATMCRC, input);
  }

  /* get the division remainder from the CRC register named "mySMBusCRC" and
   * print its value as the final CRC result */
  printf("mySMBusCRC result: %.2x\n", mySMBusCRC);

  /* we could use the polynomial division remainder from the "myATMCRC"
   * register; or per ATM User-Network Interface Specification XOR the
   * remainder with "01010101" to produce implementation specific
   * final CRC */
  printf("myATMCRC remainder: %.2x\n", myATMCRC);
  printf("ITU ATM HEC CRC result: %.2x\n", 0x55 ^ myATMCRC);
}
