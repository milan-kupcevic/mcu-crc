/*
 *
 * MCU CRC-16-CCITT usage example
 *
 *
 * Copyright (C) 2017  Milan Kupcevic
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * 2-clause BSD
 *
 */

/* *****************************************************************
 *
 * USAGE EXAMPLE
 * -------------
 * Compile this example and test it by typing:
 *
 *  printf "123456789" | ./mcu-crc16-ccitt-example
 *
 *  myKermitCRC result: 2189
 *  myX25CRC remainder: 6f91
 *  myX25CRC result: 906e
 *
 * or in Windows command shell:
 *
 *  echo | set /p ="123456789" | mcu-crc16-ccitt-example.exe
 *
 *  myKermitCRC result: 2189
 *  myX25CRC remainder: 6f91
 *  myX25CRC result: 906e
 *
 */

/* This example needs stdio.h to allow for reading data from stdin and
 * printing the division remainder and CRC result out. The stdio.h is
 * not needed for the CRC computing itself.  */
#include <stdio.h>

/* include the MCU CRC-16-CCITT implementation */
#include "mcu-crc16-ccitt.h"

int
main()
{
  int input;

  /* set the CRC register named "myKermitCRC" to default initial state */
  unsigned short int myKermitCRC = 0x0000;

  /* set the CRC register named "myX25CRC" to implementation specific initial
   * value to achieve ITU_T X.25 specified CRC behaviour */
  unsigned short int myX25CRC = 0xffff;

  while ((input = getchar()) != EOF)
  {
    /* compute the data payload byte by byte using "myKermitCRC" register */
    crc16_ccitt_compute(&myKermitCRC, input);

    /* compute the data payload byte by byte using "myX25CRC" register */
    crc16_ccitt_compute(&myX25CRC, input);
  }

  /* get the division remainder from the CRC register named "myKermitCRC" and
   * print its value as the final CRC result */
  printf("myKermitCRC result: %.4x\n", 0xffff & myKermitCRC);

  /* we could use the polynomial division remainder from the "myX25CRC"
   * register; or per ITU-T X.25 2.2.7.4 recommendation produce implementation
   * specific final CRC result as ones complement of the remainder */
  printf("myX25CRC remainder: %.4x\n", 0xffff & myX25CRC);
  printf("myX25CRC result: %.4x\n", 0xffff &~ myX25CRC);
}
